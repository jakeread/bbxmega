# Breadboard XMEGA

A tiny devkit for XMEGA XPeriments.

![complete](https://gitlab.cba.mit.edu/jakeread/bbxmega/raw/master/images/complete.jpg) 

BBXMega breaks out Port A and Port C on the XMEGA128A4U, as well as a USB on the USBDM and USBDP lines, for software USB support available with Atmel Studio 7 and ASF. To boos USB Serial Communication, see [this](http://wa4bvy.com/xmega_doxy/udi_cdc_quickstart.html) quickstart and [this](http://www.atmel.com/Images/doc8447.pdf) Atmel Doc.

Programs via PDI / Atmel ICE. Possible to load a USB bootloader!

GND and 3V3 broken out on power rail of breadboard.

![board](https://gitlab.cba.mit.edu/jakeread/bbxmega/raw/master/images/board.jpg) 

![schematic](https://gitlab.cba.mit.edu/jakeread/bbxmega/raw/master/images/schematic.jpg) 

See /circuit for Eagle files and .png traces / outline for manufacture using [Mods](mods.cba.mit.edu).